# BET Project

BET Project application - developed and tested using google chrome

- Execute following scripts
  1. Create script(Creating tables) SQL\CTab\
  2. Alter script(Enable DB broker) SQL\ATab\
  3. Indexes  SQL\Indexes
  4. Stored procedures 	SQL\SProc
  
- Change database values ('data source' and 'catalog') in the following files  
	1. BETProject\Web.config
	2. BETProject.Models\App.config

NB: Created 3 connection strings in the Web.config file see below:
	1. DefaultConnection used for connecting the DB with ASP Identity for access
	2. HollywoodTestEntities used for application tables
	3. SignalRConnection used for connecting to application tables when using SignalRConnection
	
	