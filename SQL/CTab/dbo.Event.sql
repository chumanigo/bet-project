SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Event](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_TournamentID] [bigint] NOT NULL,
	[EventName] [varchar](100) NOT NULL,
	[EventNumber] [smallint] NOT NULL,
	[EventDateTime] [datetime] NULL,
	[EventEndDateTime] [datetime] NULL,
	[AutoClose] [bit] NOT NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK_Event_Tournament] FOREIGN KEY([FK_TournamentID])
REFERENCES [dbo].[Tournament] ([TournamentID])
GO

ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK_Event_Tournament]
GO