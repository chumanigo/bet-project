SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EventDetail](
	[EventDetailID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_EventID] [bigint] NOT NULL,
	[FK_EventDetailStatusID] [smallint] NOT NULL,
	[EventDetailName] [varchar](50) NOT NULL,
	[EventDetailNumber] [smallint] NOT NULL,
	[EventDetailOdd] [decimal](18, 7) NOT NULL,
	[FinishingPosition] [smallint] NULL,
	[FirstTimer] [bit] NOT NULL,
 CONSTRAINT [PK_EventDetail] PRIMARY KEY CLUSTERED 
(
	[EventDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[EventDetail]  WITH CHECK ADD  CONSTRAINT [FK_EventDetail_Event] FOREIGN KEY([FK_EventID])
REFERENCES [dbo].[Event] ([EventID])
GO

ALTER TABLE [dbo].[EventDetail] CHECK CONSTRAINT [FK_EventDetail_Event]
GO

ALTER TABLE [dbo].[EventDetail]  WITH CHECK ADD  CONSTRAINT [FK_EventDetail_EventDetailStatus] FOREIGN KEY([FK_EventDetailStatusID])
REFERENCES [dbo].[EventDetailStatus] ([EventDetailStatusID])
GO

ALTER TABLE [dbo].[EventDetail] CHECK CONSTRAINT [FK_EventDetail_EventDetailStatus]
GO