-- Check if stored procedure exists, and delete it if it does.
IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = OBJECT_ID('dbo.spTournament_SEL_All')
              AND OBJECTPROPERTY(id, 'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE dbo.spTournament_SEL_All
  END
GO
-- Set quoted identifiers on, in case any column is a reserved word.
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spTournament_SEL_All
AS
SET NOCOUNT ON 
  SELECT
   TournamentID
  ,TournamentName 
FROM dbo.Tournament WITH(NOLOCK)
