-- Check if stored procedure exists, and delete it if it does.
IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = OBJECT_ID('dbo.spEventDetail_SEL_All')
              AND OBJECTPROPERTY(id, 'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE dbo.spEventDetail_SEL_All
  END
GO
-- Set quoted identifiers on, in case any column is a reserved word.
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spEventDetail_SEL_All
AS
SET NOCOUNT ON 
  SELECT
   EventDetailID
  ,FK_EventID
  ,FK_EventDetailStatusID
  ,EventDetailName
  ,EventDetailNumber
  ,EventDetailOdd
  ,FinishingPosition
  ,FirstTimer
FROM dbo.EventDetail WITH(NOLOCK)
