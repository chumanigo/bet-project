-- Check if stored procedure exists, and delete it if it does.
IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = OBJECT_ID('dbo.spEvent_TournamentID')
              AND OBJECTPROPERTY(id, 'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE dbo.spEvent_TournamentID
  END
GO
-- Set quoted identifiers on, in case any column is a reserved word.
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spEvent_TournamentID
(
  @FK_TournamentID bigint
)
AS
SET NOCOUNT ON 
  SELECT
   EventID 
  ,FK_TournamentID
  ,EventName
  ,EventNumber
  ,EventDateTime
  ,EventEndDateTime
  ,AutoClose
FROM dbo.Event  WITH(NOLOCK)
WHERE FK_TournamentID = @FK_TournamentID
GO 