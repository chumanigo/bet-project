ALTER DATABASE HollywoodTest SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE

-- To check weather broker is enable or not 
SELECT is_broker_enabled FROM sys.databases WHERE name = 'HollywoodTest'

-- To kill all the database running process
USE master
GO

DECLARE @dbname sysname
SET @dbname = 'HollywoodTest'

DECLARE @spid int

SELECT @spid = min(spid) from master.dbo.sysprocesses where dbid = db_id(@dbname)
WHILE @spid IS NOT NULL
BEGIN
	EXECUTE ('KILL ' + @spid)

	SELECT @spid = min(spid) from master.dbo.sysprocesses where dbid = db_id(@dbname) AND spid > @spid
END

ALTER DATABASE HollywoodTest SET ENABLE_BROKER