﻿using BETProject.Models;
using BETProject.WebServices.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BETProject.WebServices
{
    public class DataAccess : IDataAccess
    {
        #region Properties
        private HollywoodTestEntities _hollywoodTestEntities { get; set; }
        #endregion
               

        #region Constructor

        public DataAccess()
        {
            _hollywoodTestEntities = new HollywoodTestEntities();
        }

        #endregion

        public HollywoodTestEntities GetContext()
        {
            return _hollywoodTestEntities;
        }

        #region Tournament
        public IEnumerable<Tournament> GetAllTournaments()
        {
            var data = _hollywoodTestEntities.Database.SqlQuery<Tournament>("dbo.spTournament_SEL_All").ToList<Tournament>();
            return data;
        }

        public Tournament GetTournament(int id)
        {
            return _hollywoodTestEntities.Tournaments.Find(id);
        }

        #endregion

        #region Event
        public IEnumerable<Event> GetAllEvents()
        {
            var data = _hollywoodTestEntities.Events.ToList();
            return data;
        }


        public Event GetEvent(int id)
        {
            return _hollywoodTestEntities.Events.Find(id);            
        }

        #endregion

        #region EventDetail
        public IEnumerable<EventDetail> GetEventDetails()
        {
            return _hollywoodTestEntities.EventDetails.ToList();            
        }

        public EventDetail GetEventDetail(int id)
        {
            return _hollywoodTestEntities.EventDetails.Find(id);
        }

        #endregion

        #region EventDetailStatus

        public IEnumerable<EventDetailStatu> GetEventDetailStatuses()
        {
            return _hollywoodTestEntities.EventDetailStatus.ToList();
        } 

        public EventDetailStatu GetEventDetailStatus(int id)
        {
            return _hollywoodTestEntities.EventDetailStatus.Find(id);
        }

        public IEnumerable<Event> GetTournamentEvents(int tournamentID)
        {
            var tounament = GetTournament(tournamentID);
            var eventIds = _hollywoodTestEntities.Events
                .Where(p => p.FK_TournamentID == tournamentID)
                .Select(p => p.EventID)
                .ToArray();

            return GetAllEvents();
        }

        //public Tournament GetTournamentEvents(int tournamentID)
        //{
        //    var data = _hollywoodTestEntities.Tournaments.Include("Events").FirstOrDefault(x => x.TournamentID == tournamentID);
        //    return data;
        //}

        public IEnumerable<EventDetail> GetEventDetailsList(int eventID)
        {
            var data = _hollywoodTestEntities.EventDetails.Where(x => x.FK_EventID == eventID);
            return data;            
        }

        #endregion



    }
}