﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BETProject.WebServices.Models
{
    public class Tournament
    {
        public long TournamentID { get; set; }
        public string TournamentName { get; set; }
    }
}