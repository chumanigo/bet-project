﻿using BETProject.Models;
using BETProject.WebServices.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace BETProject.WebServices.Controllers
{
    public class EventDetailsController : ApiController
    {
        #region Constructor
        private IDataAccess _repository { get; set; }

        public EventDetailsController(IDataAccess dataAccess)
        {
            _repository = dataAccess;
        }
        #endregion

        public IEnumerable<EventDetail> GetAllEventDetails()
        {
            return _repository.GetEventDetails();
        }

        [ResponseType(typeof(EventDetail))]
        [HttpPost]
        public async Task<IHttpActionResult> Create(EventDetail eventDetailObj)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.GetContext().EventDetails.Add(eventDetailObj);
            await _repository.GetContext().SaveChangesAsync();

            var dto = new EventDetail()
            {
                FK_EventDetailStatusID = eventDetailObj.FK_EventDetailStatusID,
                FK_EventID = eventDetailObj.FK_EventID,
                EventDetailName = eventDetailObj.EventDetailName,
                EventDetailNumber = eventDetailObj.EventDetailNumber,
                EventDetailOdd = eventDetailObj.EventDetailOdd,
                FinishingPosition = eventDetailObj.FinishingPosition                
            };

            return CreatedAtRoute("DefaultApi", new { id = eventDetailObj.EventDetailID }, dto);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetEventDetail(int id)
        {
            var evnt = await _repository.GetContext().EventDetails.FindAsync(id);
            if (evnt == null)
            {
                return NotFound();
            }
            return Ok(evnt);
        }

        [ResponseType(typeof(EventDetail))]
        [HttpPut]
        public async Task<IHttpActionResult> Update(EventDetail eventDetailObj)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var datamodel = _repository.GetContext().EventDetails.Find(eventDetailObj.EventDetailID);
            if (datamodel != null)
            {
                datamodel.FK_EventID = eventDetailObj.FK_EventID;
                datamodel.FK_EventDetailStatusID = eventDetailObj.FK_EventDetailStatusID;
                datamodel.EventDetailName = eventDetailObj.EventDetailName;
                datamodel.EventDetailNumber = eventDetailObj.EventDetailNumber;
                datamodel.EventDetailOdd = eventDetailObj.EventDetailOdd;
                datamodel.EventDetailStatu = eventDetailObj.EventDetailStatu;
                await _repository.GetContext().SaveChangesAsync();
            }

            return CreatedAtRoute("DefaultApi", new { id = eventDetailObj.EventDetailID }, datamodel);
        }

        [Route("api/EventDetailsList/{eventID}")]
        [HttpGet]
        public IEnumerable<EventDetail> GetEventDetails(int eventID)
        {
            var eventDetailObj = (from c in _repository.GetContext().EventDetails.ToList()
                            where c.FK_EventID == eventID
                            select new EventDetail()
                            {
                                EventDetailID = c.EventDetailID,
                                FK_EventDetailStatusID = c.FK_EventDetailStatusID,
                                FK_EventID = c.FK_EventID,
                                EventDetailName = c.EventDetailName,
                                EventDetailNumber = c.EventDetailNumber,
                                EventDetailOdd = c.EventDetailOdd,
                                FinishingPosition = c.FinishingPosition,
                                FirstTimer = c.FirstTimer

                            }).ToList();

            return eventDetailObj;
        }

        [ResponseType(typeof(EventDetail))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            EventDetail eventDetailObj = await _repository.GetContext().EventDetails.FindAsync(id);
            if (eventDetailObj == null)
            {
                return NotFound();
            }

            _repository.GetContext().EventDetails.Remove(eventDetailObj);
            await _repository.GetContext().SaveChangesAsync();

            return Ok(eventDetailObj);
        }

    }
}
