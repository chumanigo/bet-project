﻿using BETProject.Models;
using BETProject.WebServices.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace BETProject.WebServices.Controllers
{
    public class TournamentsController : ApiController
    {
        #region Constructor
        private IDataAccess _repository { get; set; }

        public TournamentsController(IDataAccess dataAccess)
        {
            _repository = dataAccess;
        }
        #endregion

        public IEnumerable<Tournament> GetAllTournaments()
        {
            return _repository.GetAllTournaments();
        }

        [ResponseType(typeof(Tournament))]
        [HttpGet]
        public async Task<IHttpActionResult> GetTournament(int id)
        {

            var tournament = await _repository.GetContext().Tournaments.FindAsync(id);
            if (tournament == null)
            {
                return NotFound();
            }
            return Ok(tournament);
        }

        [ResponseType(typeof(Tournament))]
        [HttpPost]
        public async Task<IHttpActionResult> Create(Tournament tournament)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.GetContext().Tournaments.Add(tournament);
            await _repository.GetContext().SaveChangesAsync();

            var dto = new Tournament()
            {
                TournamentName = tournament.TournamentName
            };

            return CreatedAtRoute("DefaultApi", new { id = tournament.TournamentID }, dto);
        }

        [ResponseType(typeof(Tournament))]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Tournament tournament)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var datamodel = _repository.GetContext().Tournaments.Find(tournament.TournamentID);
            if (datamodel != null)
            {
                datamodel.TournamentName = tournament.TournamentName;
                await _repository.GetContext().SaveChangesAsync();
            }

            return CreatedAtRoute("DefaultApi", new { id = tournament.TournamentID }, datamodel);
        }

        [ResponseType(typeof(Tournament))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            Tournament tournament = await _repository.GetContext().Tournaments.FindAsync(id);
            if (tournament == null)
            {
                return NotFound();
            }

            // Get event details
            var events = tournament.Events.Where(x => x.FK_TournamentID == id);

            // Remove event details
            foreach (Event item in events)
            {
                _repository.GetContext().EventDetails.RemoveRange(item.EventDetails);
            }

            // Delete tournament events
            _repository.GetContext().Events.RemoveRange(tournament.Events);

            // Delete Tournament
            _repository.GetContext().Tournaments.Remove(tournament);
            await _repository.GetContext().SaveChangesAsync();

            return Ok(tournament);
        }
    }
}
