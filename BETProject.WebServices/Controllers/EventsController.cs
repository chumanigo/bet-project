﻿using BETProject.Models;
using BETProject.WebServices.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace BETProject.WebServices.Controllers
{
    public class EventsController : ApiController
    {
        #region Constructor
        private IDataAccess _repository { get; set; }

        public EventsController(IDataAccess dataAccess)
        {
            _repository = dataAccess;
        }
        #endregion

        public IEnumerable<Event> GetAllEvents()
        {
            return _repository.GetAllEvents();
        }


        public IEnumerable<Event> GetEventsTEST(int tournamentID)
        {
            var events = _repository.GetTournamentEvents(tournamentID);

            return events;
        }

        [Route("api/TournamentEvents/{tournamentID}")]
        [HttpGet]
        public IEnumerable<Event> GetEvents(int tournamentID)
        {
            var eventObj = (from c in _repository.GetContext().Events.ToList()
                            where c.FK_TournamentID == tournamentID
                            select new Event()
                            {
                                FK_TournamentID = c.FK_TournamentID,
                                EventID = c.EventID,
                                EventName = c.EventName,
                                EventNumber = c.EventNumber,
                                EventDateTime = c.EventDateTime,
                                EventEndDateTime = c.EventEndDateTime,
                                AutoClose = c.AutoClose

                            }).ToList();

            return eventObj;
        }

        [ResponseType(typeof(Event))]
        [HttpGet]
        public async Task<IHttpActionResult> GetEvent(int id)
        {
            var eventObj = await _repository.GetContext().Events.FindAsync(id);
            if (eventObj == null)
            {
                return NotFound();
            }
            return Ok(eventObj);
        }

        [ResponseType(typeof(Event))]
        [HttpPost]
        public async Task<IHttpActionResult> Create(Event eventObj)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.GetContext().Events.Add(eventObj);
            await _repository.GetContext().SaveChangesAsync();

            var dto = new Event()
            {
                FK_TournamentID = eventObj.FK_TournamentID,
                EventName = eventObj.EventName,
                EventNumber = eventObj.EventNumber,
                EventDateTime = eventObj.EventDateTime,
                EventEndDateTime = eventObj.EventEndDateTime,
                AutoClose = eventObj.AutoClose
            };

            return CreatedAtRoute("DefaultApi", new { id = eventObj.EventID }, dto);
        }

        [ResponseType(typeof(Event))]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Event eventObj)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var datamodel = _repository.GetContext().Events.Find(eventObj.EventID);
            if (datamodel != null)
            {
                datamodel.FK_TournamentID = eventObj.FK_TournamentID;
                datamodel.EventName = eventObj.EventName;
                datamodel.EventNumber = eventObj.EventNumber;
                datamodel.EventDateTime = eventObj.EventEndDateTime;
                datamodel.EventEndDateTime = eventObj.EventEndDateTime;
                datamodel.AutoClose = eventObj.AutoClose;
                await _repository.GetContext().SaveChangesAsync();
            }

            return CreatedAtRoute("DefaultApi", new { id = eventObj.EventID }, datamodel);
        }

        [ResponseType(typeof(Event))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            Event eventObj = await _repository.GetContext().Events.FindAsync(id);
            if (eventObj == null)
            {
                return NotFound();
            }

            // Remove event details
            _repository.GetContext().EventDetails.RemoveRange(eventObj.EventDetails);

            _repository.GetContext().Events.Remove(eventObj);
            await _repository.GetContext().SaveChangesAsync();

            return Ok(eventObj);
        }

    }
}
