﻿using BETProject.Models;
using BETProject.WebServices.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace BETProject.WebServices.Controllers
{
    public class EventDetailStatusController : ApiController
    {
        #region Constructor
        private IDataAccess _repository { get; set; }

        public EventDetailStatusController(IDataAccess dataAccess)
        {
            _repository = dataAccess;
        }
        #endregion

        public IEnumerable<EventDetailStatu> GetAllEventDetailStatus()
        {
            return _repository.GetContext().EventDetailStatus.ToList();
        }

        [ResponseType(typeof(EventDetailStatu))]
        public async Task<IHttpActionResult> GetEventDetailStatus(int id)
        {
            var evnt = await _repository.GetContext().EventDetailStatus.FindAsync(id);
            if (evnt == null)
            {
                return NotFound();
            }
            return Ok(evnt);
        }
    }
}
