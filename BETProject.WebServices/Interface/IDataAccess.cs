﻿using BETProject.Models;
using System;
using System.Collections.Generic;

namespace BETProject.WebServices.Interface
{
    public interface IDataAccess
    {
        HollywoodTestEntities GetContext();
        IEnumerable<Tournament> GetAllTournaments();
        IEnumerable<Event> GetAllEvents();
        IEnumerable<EventDetail> GetEventDetails();
        IEnumerable<EventDetailStatu> GetEventDetailStatuses();
        //Tournament GetTournamentEvents(int tournamentID);
        IEnumerable<Event> GetTournamentEvents(int tournamentID);
        IEnumerable<EventDetail> GetEventDetailsList(int eventID);

        //Tournament Get();
        //Tournament Push();
        //Tournament Post();


        Tournament GetTournament(int id);
        Event GetEvent(int id);
        EventDetail GetEventDetail(int id);
        EventDetailStatu GetEventDetailStatus(int id);

    }
}