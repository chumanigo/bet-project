﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BETProject.Models
{
    public class EventDetailViewModel
    {
        public long EventDetailID { get; set; }
        public long FK_EventID { get; set; }
        public short FK_EventDetailStatusID { get; set; }
        public string EventDetailName { get; set; }
        public short EventDetailNumber { get; set; }
        public decimal EventDetailOdd { get; set; }
        public Nullable<short> FinishingPosition { get; set; }
        public bool FirstTimer { get; set; }
        public string EventDetailStatusName { get; set; }

        //public virtual Event Event { get; set; }
        //public virtual EventDetailStatu EventDetailStatu { get; set; }
    }
}