﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BETProject.Models
{
    public class EventDetailStatusViewModel
    {
        public short EventDetailStatusID { get; set; }
        public string EventDetailStatusName { get; set; }
    }
}