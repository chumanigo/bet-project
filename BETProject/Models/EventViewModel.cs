﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BETProject.Models
{
    public class EventViewModel
    {
        public long EventID { get; set; }
        public long FK_TournamentID { get; set; }
        public string EventName { get; set; }
        public short EventNumber { get; set; }        
        [DataType(DataType.DateTime)]
        public System.DateTime? EventDateTime { get; set; }
        [DataType(DataType.DateTime)]
        public System.DateTime? EventEndDateTime { get; set; }
        public bool AutoClose { get; set; }

        //public List<EventDetailViewModel> EventDetails { get; set; }
    }
}