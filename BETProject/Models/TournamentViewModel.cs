﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BETProject.Models
{
    public class TournamentViewModel
    {
        public long TournamentID { get; set; }
        [Display(Name = "Tournament Name")]
        public string TournamentName { get; set; }
        public List<EventViewModel> Events { get; set; }
    }
}