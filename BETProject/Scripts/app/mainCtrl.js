﻿
(function () {

    toastr.options.positionClass = 'toast-top-center'  

    app.directive('confirmClick', function ($window) {
        var i = 0;
        return {
            restrict: 'A',
            priority: 1,
            compile: function (tElem, tAttrs) {
                var fn = '$$confirmClick' + i++,
                    _ngClick = tAttrs.ngClick;
                tAttrs.ngClick = fn + '($event)';

                return function (scope, elem, attrs) {
                    var confirmMsg = attrs.confirmClick || 'Are you sure?';

                    scope[fn] = function (event) {
                        if ($window.confirm(confirmMsg)) {
                            scope.$eval(_ngClick, { $event: event });
                        }
                    };
                };
            }
        };
    });

    app.controller("tournamentCtrl", function ($scope, $http, $route) {
 
        $scope.getTournament = function (tournamentID) {                
            $http({
                method: 'GET',
                dataType: 'JSON',
                params: {
                    'id': tournamentID
                },
                url: '/Tournament/GetTournament'
            }).then(function (response) {
                $scope.tournament = response.data;                
            });
        }

        $scope.getTournaments = function () {            
            $http({
                method: 'GET',
                dataType: 'JSON',
                url: '/Tournament/GetAll'
            }).then(function (response) {
                    $scope.tournaments = response.data;
                });  
        }

        $scope.saveTournament = function () {
            $scope.datas = {};            

            $scope.datas.TournamentName = $scope.TournamentName
            $.ajax({
                type: "POST",
                url: '/Tournament/Create',
                data: $scope.datas,
                success: function (result) {
                    if (result.Success == true) {
                        toastr.success(result.Message);
                        
                        $('#createTounamentModal').modal('hide');
                        $('.modal-body').find('textarea,input,select').val('');
                        $('body').removeClass('modal-dialog');      
                        $('.modal-backdrop').remove();

                        //-- reload data
                        $scope.getTournaments();
                    }
                    else {
                        toastr.error(result.Message);
                    }
                },
                error: function (xhr, error) {
                    toastr.error("An error occured while trying to creating tournament. Status: " + xhr.status + ", Error: " + xhr.statusText);
                }
            });
        };

        $scope.updateTournament = function (tournamentid) {
            $scope.data = {};

            $scope.data.TournamentID = tournamentid
            $scope.data.TournamentName = $scope.tournament.TournamentName

            $.ajax({
                type: "POST",
                url: '/Tournament/Update',
                data: $scope.data,
                success: function (result) {
                    if (result.Success == true) {
                        toastr.success(result.Message);
                    }
                    else {
                        toastr.error(result.Message);
                    }
                },
                error: function (xhr, error) {
                    toastr.error("An error occured while trying to updating tournament. Status: " + xhr.status + ", Error: " + xhr.statusText);
                }
            });

      
        };

        $scope.editTournament = function (id) {         
            //$scope.getTournament(id);
            location.href = "/Tournament/Edit/" + id;
        }

        $scope.deleteTournament = function (id) {
            
            var tournamentData =
            {
                TournamentID: id
            }
            $.ajax({
                type: "POST",
                url: '/Tournament/Delete',
                data: tournamentData,
                success: function (result) {
                    if (result.Success == true) {
                        toastr.success(result.Message);

                        //-- reload data
                        $scope.getTournaments();                        
                    }
                    else {
                        toastr.error(result.Message);
                    }
                },
                error: function (xhr, error) {
                    toastr.error("An error occured while trying to delete tournament. Status: " + xhr.status + ", Error: " + xhr.statusText);
                }
            });
        }

    });

    app.controller("eventCtrl", function ($scope, $http, $filter) {  
        $scope.dateFormat = "yyyy-MM-dd HH:mm:ss";

         // convert Json date
        function parseJsonDate(jsonDateString) {
            if (jsonDateString == null)
                return null;

            return new Date(parseInt(jsonDateString.replace('/Date(', '')));            
        }

        $scope.getEvent = function (eventID) {            

            $http({
                method: 'GET',
                dataType: 'JSON',
                params: {
                    'id': eventID
                },
                url: '/Event/GetEvent'
            }).then(function (response) {                
                $scope.event = response.data;
                $scope.event.EventDateTime = parseJsonDate($scope.event.EventDateTime);         
                $scope.event.EventEndDateTime = parseJsonDate($scope.event.EventEndDateTime);                                  
            });
        }

        $scope.getEvents = function (eventID) {               
            $scope.eventID = eventID;            
            $http({
                method: 'GET',
                dataType: 'JSON',
                url: '/Event/GetAll'
            }).then(function (response) {
                $scope.events = response.data;                                
            });
        }

        $scope.getTournamentEvents = function (tournamentID) { 
            $scope.TournamentID = tournamentID;
            $http({
                method: 'GET',
                dataType: 'JSON',
                params: {
                    'id': tournamentID
                },
                url: '/Event/GetTournamentEvents'
            }).then(function (response) {
                $scope.events = response.data;

                // convert Json date to allow display
                angular.forEach($scope.events, function (event, index) {                    
                    event.EventDateTime = parseJsonDate(event.EventDateTime);
                    event.EventEndDateTime = parseJsonDate(event.EventEndDateTime);
                }) 
                
            });
        }       

        $scope.saveEvent = function () {            
            $scope.datas = {};    

            $scope.EventDateTime = $filter('date')($scope.EventDateTime, $scope.dateFormat);
            $scope.EventEndDateTime = $filter('date')($scope.EventEndDateTime, $scope.dateFormat);

            $scope.datas.EventName = $scope.EventName;
            $scope.datas.EventNumber = $scope.EventNumber;
            $scope.datas.FK_TournamentID = $scope.TournamentID;
            $scope.datas.EventDateTime = $scope.EventDateTime;
            $scope.datas.EventEndDateTime = $scope.EventEndDateTime;
            $scope.datas.AutoClose = $scope.AutoClose;

            $.ajax({
                type: "POST",
                url: '/Event/Create',
                data: $scope.datas,
                success: function (result) {
                    if (result.Success == true) {
                        toastr.success(result.Message);
                        // close modal
                        $('#createEventModal').modal('hide');
                        $('.modal-body').find('textarea,input,select').val('');
                        $('body').removeClass('modal-dialog');
                        $('.modal-backdrop').remove();

                        //-- reload data
                        $scope.getTournamentEvents($scope.TournamentID);
                    }
                    else {
                        toastr.error(result.Message);
                    }
                },
                error: function (xhr, error) {
                    toastr.error("An error occured while trying to creating tournament. Status: " + xhr.status + ", Error: " + xhr.statusText);
                }
            });
        }; 

        $scope.updateEvent = function (eventID) {
            $scope.datas = {};

            $scope.event.EventDateTime = $filter('date')($scope.event.EventDateTime, $scope.dateFormat);
            $scope.event.EventEndDateTime = $filter('date')($scope.event.EventEndDateTime, $scope.dateFormat);

            $scope.datas.EventID = eventID;
            $scope.datas.EventName = $scope.event.EventName
            $scope.datas.EventNumber = $scope.event.EventNumber;
            $scope.datas.FK_TournamentID = $scope.event.FK_TournamentID;
            $scope.datas.EventDateTime = $scope.event.EventDateTime;
            $scope.datas.EventEndDateTime = $scope.event.EventEndDateTime;
            $scope.datas.AutoClose = $scope.event.AutoClose;            
     
            $.ajax({
                type: "POST",
                url: '/Event/Update',
                data: $scope.datas,
                success: function (result) {
                    if (result.Success == true) {
                        toastr.success(result.Message);
                    }
                    else {
                        toastr.error(result.Message);
                    }
                },
                error: function (xhr, error) {
                    toastr.error("An error occured while trying to updating tournament. Status: " + xhr.status + ", Error: " + xhr.statusText);
                }
            });


        };

        $scope.editEvent = function (id) {
            location.href = "/Event/Edit/" + id;
        }

        $scope.deleteEvent = function (id) {
            var eventData =
            {
                EventID: id
            }
            $.ajax({
                type: "POST",
                url: '/Event/Delete',
                data: eventData,
                success: function (result) {
                    if (result.Success == true) {
                        toastr.success(result.Message);

                        //-- reload data
                        $scope.getTournamentEvents($scope.TournamentID);                        
                    }
                    else {
                        toastr.error(result.Message);                        
                    }
                },
                error: function (xhr, error) {
                    toastr.error("An error occured while trying to delete event. Status: " + xhr.status + ", Error: " + xhr.statusText);
                }
            });
        }
    });

    app.controller("eventDetailCtrl", function ($scope, $http) {      
        $scope.dateFormat = "yyyy-MM-dd HH:mm:ss";
        $scope.eventStatusList = [];

        var eventhub = $.connection.eventDetailHub;

        function init() {
            console.log("Started Signal hub");
        }

        eventhub.client.notify = function (eventDetail) {
            $scope.$apply(function () {
                $scope.eventDetails = JSON.parse(JSON.stringify(eventDetail));
            });
        }

        $.connection.hub.start().then(init);

        $scope.getEventStatusList = function () {
            $http({
                method: 'GET',
                dataType: 'JSON',  
                url: '/EventDetail/GetAllEventDetailStatus'
            }).then(function (response) {
                $scope.eventStatusList = response.data;                
            });
        }

        $scope.validSelection = function (selectedValue) {
            if (selectedValue != "" && selectedValue != undefined)
                return true;
            else
                toastr.error('Please Select Event Detail Status');
            return false;
        }
        
        $scope.getEventDetail = function (eventDetailID) {             
            $http({
                method: 'GET',
                dataType: 'JSON',
                params: {
                    'id': eventDetailID
                },
                url: '/EventDetail/GetEventDetail'
            }).then(function (response) {
                $scope.getEventStatusList();
                $scope.eventDetail = response.data;     
            });
        }

        $scope.getEventDetails = function (eventDetailID) {
            $scope.eventDetailID = eventDetailID; 
            $http({
                method: 'GET',
                dataType: 'JSON',
                url: '/EventDetail/GetAll'
            }).then(function (response) {
                $scope.eventDetails = response.data;                
            });
        }

        $scope.getEventDetailsList = function (eventID) {
            $scope.EventID = eventID;
            $http({
                method: 'GET',
                dataType: 'JSON',
                params: {
                    'id': eventID
                },
                url: '/EventDetail/GetEventDetailsList'
            }).then(function (response) {
                $scope.eventDetails = response.data;
                $scope.getEventStatusList();    
            });
        } 

        $scope.getStatusName = function (input) { 
            angular.forEach($scope.eventStatusList, function (value) { 
                if (value.EventDetailStatusID === input) {
                    return value.EventDetailStatusName;
                }
            })           
        }

        $scope.saveEventDetail = function () {

            if (!$scope.validSelection($scope.EventDetailStatusID))
                return;
            
            $scope.datas = {};
            $scope.datas.EventDetailName = $scope.EventDetailName;
            $scope.datas.EventDetailNumber = $scope.EventDetailNumber;
            $scope.datas.FK_EventDetailStatusID = $scope.EventDetailStatusID
            $scope.datas.FK_EventID = $scope.EventID;
            $scope.datas.EventDetailOdd = $scope.EventDetailOdd;
            $scope.datas.FinishingPosition = $scope.FinishingPosition;
            $scope.datas.FirstTimer = $scope.FirstTimer;

            $.ajax({
                type: "POST",
                url: '/EventDetail/Create',
                data: $scope.datas,
                success: function (result) {
                    if (result.Success == true) {
                        toastr.success(result.Message);

                        // close modal
                        $('#eventDetailModal').modal('hide');
                        $('.modal-body').find('textarea,input,select').val('');
                        $('body').removeClass('modal-dialog');
                        $('.modal-backdrop').remove();

                        //-- reload data
                        $scope.getEventDetailsList($scope.EventID);
                    }
                    else {
                        toastr.error(result.Message);
                    }
                },
                error: function (xhr, error) {
                    toastr.error("An error occured while trying to creating tournament. Status: " + xhr.status + ", Error: " + xhr.statusText);
                }
            });     
        };

        $scope.editEventDetail = function (id) {            
            location.href = "/EventDetail/Edit/" + id;
        }

        $scope.updateEventDetail = function (eventDetailID) {

            if (!$scope.validSelection($scope.eventDetail.FK_EventDetailStatusID))
                return;

            $scope.datas = {};
            $scope.datas.EventDetailID = eventDetailID;
            $scope.datas.EventDetailName = $scope.eventDetail.EventDetailName;
            $scope.datas.EventDetailNumber = $scope.eventDetail.EventDetailNumber;
            $scope.datas.FK_EventDetailStatusID = $scope.eventDetail.FK_EventDetailStatusID
            $scope.datas.FK_EventID = $scope.eventDetail.FK_EventID;
            $scope.datas.EventDetailOdd = $scope.eventDetail.EventDetailOdd;
            $scope.datas.FinishingPosition = $scope.eventDetail.FinishingPosition;
            $scope.datas.FirstTimer = $scope.eventDetail.FirstTimer;   

            $.ajax({
                type: "POST",
                url: '/EventDetail/Update',
                data: $scope.datas,
                success: function (result) {
                    if (result.Success == true) {
                        toastr.success(result.Message);
                    }
                    else {
                        toastr.error(result.Message);
                    }
                },
                error: function (xhr, error) {
                    toastr.error("An error occured while trying to updating tournament. Status: " + xhr.status + ", Error: " + xhr.statusText);
                }
            });
        };

        $scope.deleteEventDetail = function (id) {            
            var eventData =
            {
                EventDetailID: id
            }
            $.ajax({
                type: "POST",
                url: '/EventDetail/Delete',
                data: eventData,
                success: function (result) {
                    if (result.Success == true) {
                        toastr.success(result.Message);                                               
                    }
                    else {
                        toastr.error(result.Message);
                    }
                },
                error: function (xhr, error) {
                    toastr.error("An error occured while trying to delete event. Status: " + xhr.status + ", Error: " + xhr.statusText);
                }
            });
        }
    });

}).call(angular); 
