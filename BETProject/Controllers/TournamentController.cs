﻿using BETProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BETProject.Controllers
{
    [Authorize]
    public class TournamentController : Controller
    {
        readonly string baseUrl = ConfigurationManager.AppSettings["baseurl"];
        readonly string apiUrl = ConfigurationManager.AppSettings["baseurl"] + "/api/Tournaments/";

        HttpClient client;

        public TournamentController()
        {
            client = new HttpClient
            {
                BaseAddress = new Uri(apiUrl)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public ActionResult Index(int? pageNumber)
        {   
            return View();
        }  

        [HttpPost]        
        public ActionResult Edit(TournamentViewModel tournament)
        {            
            return View(tournament);
        }
                
        public async Task<ActionResult>  Edit(int id)
        {            
            TournamentViewModel tournament = new TournamentViewModel();
            HttpResponseMessage responseMessage = await client.GetAsync(apiUrl + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                tournament = JsonConvert.DeserializeObject<TournamentViewModel>(responseData);
            }
            return View(tournament);
        }

        public async Task<ActionResult> GetTournament(int id)
        {
            TournamentViewModel tournament = new TournamentViewModel();
            HttpResponseMessage responseMessage = await client.GetAsync(apiUrl + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                tournament = JsonConvert.DeserializeObject<TournamentViewModel>(responseData);
            }          

            return Json(tournament, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetAll()
        {
            List<TournamentViewModel> tournaments = new List<TournamentViewModel>();
            HttpResponseMessage responseMessage = await client.GetAsync(apiUrl);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                tournaments = JsonConvert.DeserializeObject<List<TournamentViewModel>>(responseData);
            }            
            return Json(tournaments, JsonRequestBehavior.AllowGet);            
        }

        //The Post method
        [HttpPost]
        public async Task<ActionResult> Create(TournamentViewModel tournament)
        {
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync(apiUrl, tournament);
            if (responseMessage.IsSuccessStatusCode)
            {
                return Json(new { Success = true, Message = "Successfully created tournament" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Success = false, Message = "Failed creating tournament" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Update(TournamentViewModel tournament)
        {
            HttpResponseMessage responseMessage = await client.PutAsJsonAsync(apiUrl +"/" + tournament.TournamentID, tournament);
            if (responseMessage.IsSuccessStatusCode)
            {
                return Json(new { Success = true, Message = "Successfully updated tournament" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Success = false, Message = "Failed updating tournament" }, JsonRequestBehavior.AllowGet);
        }

        //The PUT Method
        [HttpPost]
        public async Task<ActionResult> Edit(int id, TournamentViewModel tournament)
        {
            HttpResponseMessage responseMessage = await client.PutAsJsonAsync(apiUrl + "/" + id, tournament);
            if (responseMessage.IsSuccessStatusCode)
            {
                return Json(new { Success = true, Message = "Successfully updated tournament" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Success = false, Message = "Failed updating tournament" }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Delete(TournamentViewModel tournament)
        {
            if (tournament.TournamentID > 0)
            {
                HttpResponseMessage responseMessage = await client.DeleteAsync(apiUrl+"/" + tournament.TournamentID);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return Json(new { Success = true, Message ="Successfully deleted tournament" }, JsonRequestBehavior.AllowGet);
                }
                    
                return Json(new { Success = false, Message = "Error occured while deleting tournament" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, Message = "Invalid tournament object." }, JsonRequestBehavior.AllowGet);
        }

    }
}
