﻿using BETProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BETProject.Controllers
{
    [Authorize]
    public class EventController : Controller
    {
        readonly string baseUrl = ConfigurationManager.AppSettings["baseurl"];
        readonly string apiUrl = ConfigurationManager.AppSettings["baseurl"] + "/api/Events/";
        HttpClient client;

        public EventController()
        {
            client = new HttpClient
            {
                BaseAddress = new Uri(apiUrl)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // GET: Event
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Edit(int id)
        {
            EventViewModel eventObj = new EventViewModel();
            HttpResponseMessage responseMessage = await client.GetAsync(apiUrl + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                eventObj = JsonConvert.DeserializeObject<EventViewModel>(responseData);
            }
            return View(eventObj);
        }

        public async Task<ActionResult> GetEvent(int id)
        {
            EventViewModel eventObj = new EventViewModel();
            HttpResponseMessage responseMessage = await client.GetAsync(apiUrl + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                eventObj = JsonConvert.DeserializeObject<EventViewModel>(responseData);
            }
            return Json(eventObj, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetTournamentEvents(int id)
        {
            List<EventViewModel> eventModel = new List<EventViewModel>();
            HttpResponseMessage responseMessage = await client.GetAsync(baseUrl + "/api/TournamentEvents/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                eventModel = JsonConvert.DeserializeObject<List<EventViewModel>>(responseData);
            }

            return Json(eventModel, JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> GetAll()
        {
            List<EventViewModel> events = new List<EventViewModel>();
            HttpResponseMessage responseMessage = await client.GetAsync(apiUrl);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                events = JsonConvert.DeserializeObject<List<EventViewModel>>(responseData);
            }
            return Json(events, JsonRequestBehavior.AllowGet);            
        }

        //The Post method
        [HttpPost]
        public async Task<ActionResult> Create(EventViewModel eventModel)
        {
            if(!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "Please capture all required fields event" }, JsonRequestBehavior.AllowGet);
            }

            HttpResponseMessage responseMessage = await client.PostAsJsonAsync(apiUrl, eventModel);
            if (responseMessage.IsSuccessStatusCode)
            {
                return Json(new { Success = true, Message = "Successfully created event" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Success = false, Message = "Failed creating event" }, JsonRequestBehavior.AllowGet);
        }

        //The PUT Method
        [HttpPost]
        public async Task<ActionResult> Edit(int id, EventViewModel eventModel)
        {
            HttpResponseMessage responseMessage = await client.PutAsJsonAsync(apiUrl + "/" + id, eventModel);
            if (responseMessage.IsSuccessStatusCode)
            {
                return Json(new { Success = true, Message = "Successfully updated event" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Success = false, Message = "Failed updating event" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Update(EventViewModel eventObj)
        {
            HttpResponseMessage responseMessage = await client.PutAsJsonAsync(apiUrl + "/" + eventObj.EventID, eventObj);
            if (responseMessage.IsSuccessStatusCode)
            {
                return Json(new { Success = true, Message = "Successfully updated event" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Success = false, Message = "Failed updating event" }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Delete(EventViewModel eventModel)
        {
            if (eventModel.EventID > 0)
            {
                HttpResponseMessage responseMessage = await client.DeleteAsync(apiUrl + "/" + eventModel.EventID);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return Json(new { Success = true, Message = "Successfully deleted event" }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { Success = false, Message = "Error occured while deleting event" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, Message = "Invalid event object." }, JsonRequestBehavior.AllowGet);
        }
    }
}