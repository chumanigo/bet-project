﻿using BETProject.Models;
using BETProject.Signal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BETProject.Controllers
{
    public class EventDetailController : Controller
    {
        readonly string baseUrl = ConfigurationManager.AppSettings["baseurl"];
        readonly string apiUrl = ConfigurationManager.AppSettings["baseurl"] + "/api/EventDetails/";
        HttpClient client;
        NotificationComponents notificationComponent;
        List<EventDetailStatusViewModel> eventDetailStatusList = new List<EventDetailStatusViewModel>();

        public EventDetailController()
        {
            client = new HttpClient
            {
                BaseAddress = new Uri(apiUrl)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            GetAllDetailStatus();
        }

        //The Post method
        [HttpPost]
        public async Task<ActionResult> Create(EventDetailViewModel eventModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "Please capture all required fields event" }, JsonRequestBehavior.AllowGet);
            }

            HttpResponseMessage responseMessage = await client.PostAsJsonAsync(apiUrl, eventModel);
            if (responseMessage.IsSuccessStatusCode)
            {
                return Json(new { Success = true, Message = "Successfully created event detail" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Success = false, Message = "Failed creating event detail" }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetEventDetail(int id)
        {


            EventDetailViewModel eventDetail = new EventDetailViewModel();
            HttpResponseMessage responseMessage = await client.GetAsync(apiUrl + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                eventDetail = JsonConvert.DeserializeObject<EventDetailViewModel>(responseData);
            }

            //-- Only register if API returns  data
            //if (eventDetail != null)
            //{
            //    notificationComponent = new NotificationComponents();
            //    int.TryParse(eventDetail.FK_EventID.ToString(), out int eventID);
            //    notificationComponent.RegisterNotification(eventID);
            //}

            return Json(eventDetail, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetEventDetailsList(int id)
        {
            List<EventDetailViewModel> eventDetailsModel = new List<EventDetailViewModel>();
            HttpResponseMessage responseMessage = await client.GetAsync(baseUrl + "/api/EventDetailsList/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                eventDetailsModel = JsonConvert.DeserializeObject<List<EventDetailViewModel>>(responseData);
            }

            foreach (var eventDetail in eventDetailsModel)
            {
                var item = eventDetailStatusList.Where(x => x.EventDetailStatusID == eventDetail.FK_EventDetailStatusID).FirstOrDefault();
                eventDetail.EventDetailStatusName = item.EventDetailStatusName;
            }

            //-- Only register if API returns  data
            if (eventDetailsModel != null)
            {
                notificationComponent = new NotificationComponents();
                notificationComponent.RegisterNotification(id);
            }

            return Json(eventDetailsModel, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Edit(int id)
        {
            EventDetailViewModel eventDetailObj = new EventDetailViewModel();
            HttpResponseMessage responseMessage = await client.GetAsync(apiUrl + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                eventDetailObj = JsonConvert.DeserializeObject<EventDetailViewModel>(responseData);
            }
            return View(eventDetailObj);
        }

        public ActionResult GetAllEventDetailStatus()
        {
            return Json(eventDetailStatusList, JsonRequestBehavior.AllowGet);
        }

        //The PUT Method
        [HttpPost]
        public async Task<ActionResult> Update(EventDetailViewModel eventDetailObj)
        {
            HttpResponseMessage responseMessage = await client.PutAsJsonAsync(apiUrl + "/" + eventDetailObj.EventDetailID, eventDetailObj);
            if (responseMessage.IsSuccessStatusCode)
            {
                return Json(new { Success = true, Message = "Successfully updated event details" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Success = false, Message = "Failed updating event details" }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Delete(EventDetailViewModel eventDetailObj)
        {
            if (eventDetailObj.EventDetailID > 0)
            {
                HttpResponseMessage responseMessage = await client.DeleteAsync(apiUrl + "/" + eventDetailObj.EventDetailID);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return Json(new { Success = true, Message = "Successfully deleted event detail" }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { Success = false, Message = "Error occured while deleting event detail" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, Message = "Invalid event detail object." }, JsonRequestBehavior.AllowGet);
        }

        public void GetAllDetailStatus()
        {
            //EventDetailStatu
            using (var context = new HollywoodTestEntities())
            {
                var list = context.EventDetailStatus.AsNoTracking().ToList();
                foreach (var item in list)
                {
                    EventDetailStatusViewModel obj = new EventDetailStatusViewModel
                    {
                        EventDetailStatusID = item.EventDetailStatusID,
                        EventDetailStatusName = item.EventDetailStatusName
                    };
                    eventDetailStatusList.Add(obj);
                }
            }
        }
    }
}