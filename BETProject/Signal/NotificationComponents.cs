﻿using BETProject.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BETProject.Signal
{
    public class NotificationComponents
    {
        static string conString = ConfigurationManager.ConnectionStrings["SignalRConnection"].ToString();
        SqlConnection sqlcon = null;
        int EventID;

        public void RegisterNotification(int eventID)
        {
            EventID = eventID;
            string sqlCommand = @"SELECT [EventDetailID],[FK_EventID],[FK_EventDetailStatusID], [EventDetailName], [EventDetailNumber], [EventDetailOdd], [FinishingPosition], [FirstTimer] FROM [dbo].[EventDetail] WHERE [FK_EventID] = @EventID";
            using (SqlConnection con = new SqlConnection(conString))
            {
                
                SqlCommand cmd = new SqlCommand(sqlCommand, con);
                cmd.Parameters.AddWithValue("@EventID", EventID);
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd.Notification = null;
                SqlDependency sqlDep = new SqlDependency(cmd);
                sqlDep.OnChange += SqlDep_OnChange;

                //-- execute command
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                }
            }
        }

        private void SqlDep_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if(e.Type == SqlNotificationType.Change)
            {
                SqlDependency sqlDep = sender as SqlDependency;
                sqlDep.OnChange -= SqlDep_OnChange;

                //Send notification to client
                var notificationHub = GlobalHost.ConnectionManager.GetHubContext<EventDetailHub>();

                notificationHub.Clients.All.notify(GetEventDetail(EventID));
                RegisterNotification(EventID);
            }
        }

        public List<EventDetail> GetEventDetail(int eventID)
        {
            DataTable dt = new DataTable();   
            List<EventDetail> EventDetailModel = new List<EventDetail>();
            try
            {
                sqlcon = new SqlConnection(conString);
                SqlCommand cmd = new SqlCommand("spEventDetail_EventID", sqlcon);
                cmd.Parameters.AddWithValue("@EventID", eventID);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Notification = null;

                if (sqlcon.State == ConnectionState.Closed)
                    sqlcon.Open();

                using (var sqlDataReader = cmd.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        int eventDetailID, fK_EventID;
                        short fK_EventDetailStatusID, eventDetailNumber, finishingPosition = 0;
                        decimal eventDetailOdd = 0;
                        bool firstTimer;

                        string eventDetailName = sqlDataReader["EventDetailName"].ToString();
                        int.TryParse(sqlDataReader["EventDetailID"].ToString(), out eventDetailID);
                        int.TryParse(sqlDataReader["FK_EventID"].ToString(), out fK_EventID);
                        int.TryParse(sqlDataReader["EventDetailID"].ToString(), out eventDetailID);
                        short.TryParse(sqlDataReader["FK_EventDetailStatusID"].ToString(), out fK_EventDetailStatusID);
                        short.TryParse(sqlDataReader["EventDetailNumber"].ToString(), out eventDetailNumber);
                        short.TryParse(sqlDataReader["FinishingPosition"].ToString(), out finishingPosition);
                        decimal.TryParse(sqlDataReader["EventDetailOdd"].ToString(), out eventDetailOdd);
                        bool.TryParse(sqlDataReader["FirstTimer"].ToString(), out firstTimer);


                        EventDetailModel.Add(new EventDetail
                        {
                            EventDetailID = eventDetailID,
                            FK_EventID = fK_EventID,
                            FK_EventDetailStatusID = fK_EventDetailStatusID,
                            EventDetailName = eventDetailName,
                            EventDetailNumber = eventDetailNumber,
                            EventDetailOdd = eventDetailOdd,
                            FinishingPosition = finishingPosition,
                            FirstTimer = firstTimer
                        });

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return EventDetailModel;
        }
    }
}